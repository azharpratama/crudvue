import Vue from 'vue'
import Router from 'vue-router'
import AdminLayout from './views/layouts/AdminLayout'
import LoginPage from './views/pages/LoginPage'
import DashboardPage from './views/pages/DashboardPage'
import PenggunaPage from './views/pages/PenggunaPage'
import BlankPage from './views/pages/BlankPage'
import Post from './views/pages/Post'
import UsersPage from './views/pages/UsersPage'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'adminLayout',
      component: AdminLayout,
      children: [
        {
          path: '/dashboard',
          name: 'dashboardPage',
          component: DashboardPage
        },
        {
          path: '/pengguna',
          name: 'penggunaPage',
          component: PenggunaPage
        },
        {
          path: '/post',
          name: 'post',
          component: Post
        },
        {
          path: '/userspage',
          name: 'usersPage',
          component: UsersPage
        },
        {
          path: '/blank',
          name: 'blankPage',
          component: BlankPage
        }
      ]
    },
    {
      path: '/login',
      name: 'loginPage',
      component: LoginPage
    }
  ]
})
